<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Ubah Huruf</title>
  </head>
  <body>
    <?php
        function ubah_huruf($string){
        //kode di sini
            $alpha = "abcdefghijklmnopqrstuvwxyz";
            $ganti = "";
            for ($i=0; $i < strlen($string) ; $i++) {
              // code...
                $position = strrpos($alpha, $string[$i]);
                $ganti .= substr($alpha , $position + 1, 1);
            }
            return $ganti;

          }



        // TEST CASES
        echo ubah_huruf('wow'); // xpx
        echo "<br>";
        echo ubah_huruf('developer'); // efwfmpqfs
        echo "<br>";
        echo ubah_huruf('laravel'); // mbsbwfm
        echo "<br>";
        echo ubah_huruf('keren'); // lfsfo
        echo "<br>";
        echo ubah_huruf('semangat'); // tfnbohbu

    ?>
  </body>
</html>
