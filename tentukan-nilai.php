<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Tentukan Nilai</title>
  </head>
  <body>
    <?php

          function tentukan_nilai($number)
          {
          //  kode disini

            if ($number >= 85 && $number <= 100){
              return $number . "Sangat Baik";
            }
            elseif($number >= 70 && $number <= 85){
              return $number . "Baik";
            }
            elseif ($number  >= 60 && $number <= 75) {
              return $number. "Cukup";
            }
            else {
              return $number. "Kurang";
            }
            echo "<br>";

          }

          //TEST CASES
          echo tentukan_nilai(98); //Sangat Baik
          echo "<br />";
          echo tentukan_nilai(76); //Baik
          echo "<br />";
          echo tentukan_nilai(67); //Cukup
          echo "<br />";
          echo tentukan_nilai(43); //Kurang






     ?>
  </body>
</html>
